///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Christianne Young <clyyoung@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

// macros for seconds per year, day, hour, minute
#define SEC_YEAR (365 * SEC_DAY)
#define SEC_DAY (24 * SEC_HOUR)
#define SEC_HOUR (60 * SEC_MIN)
#define SEC_MIN (60)

int main(int argc, char* argv[]) {
   // define datatypes time_t
   time_t now, reference, current;

   // define difference in seconds
   double diff;

   // struct for reference date
   struct tm date = {
      .tm_sec = 0,               // seconds [0 to 59]
      .tm_min = 32,              // minutes [0 to 59]
      .tm_hour = 23,             // hours [0 to 23]
      .tm_mday = 2,              // day of the month [1 to 31]
      .tm_mon = 1,               // month [0 to 11]
      .tm_year = 2021 - 1900    // number of years since 1900
   }; 

   // change struct tm to time_t
   reference = mktime(&date);
   // print reference time
   printf("Reference time:  %s", asctime(&date));
   
   // infinite loop counting down/up
   while(1){
      // declare integers
      int years, days, hours, minutes, seconds;
   
      // get current time in time_t datatype
      current = time(&now);

      // calculate difference in seconds between reference and current time
      // take absolute value to ignore negative numbers
      diff = abs(difftime(reference, current));

      // calculate years, days, hours, minutes, and seconds from difference in seconds
      // update variable diff to reflect leftover seconds
      years = diff / SEC_YEAR;
      diff = diff - (years * SEC_YEAR);
      days = diff / SEC_DAY;
      diff = diff - (days * SEC_DAY);
      hours = diff / SEC_HOUR;
      diff = diff - (hours * SEC_HOUR);
      minutes = diff / SEC_MIN;
      seconds = diff - (minutes * SEC_MIN);

      // print updated time difference
      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", years, days, hours, minutes, seconds);
      
      // wait one second
      sleep(1);
   }

   return 0;
}
